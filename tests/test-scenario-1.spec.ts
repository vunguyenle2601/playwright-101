import { test, expect } from "@playwright/test";

const lambdaTestSeleniumPlaygroundUrl = "https://www.lambdatest.com/selenium-playground";
const simpleFormDemoSelector = "//a[normalize-space()='Simple Form Demo']";
const enterMessageInputSelector = "//input[@id='user-message']";
const buttonGetCheckedValue = "(//button[normalize-space()='Get Checked Value'])[1]";
const getMessageTextSelector = "(//p[@id='message'])[1]";
const dradDropSlider = "//a[normalize-space()='Drag & Drop Sliders']";


test("Test Scenario 1 - Simple Form Demo", async ({ page }) => {
  // Open LambdaTest's Selenium Playground
  await page.goto(lambdaTestSeleniumPlaygroundUrl);

  // Click "Simple Form Demo" link
  await page.click(simpleFormDemoSelector);

  // Validate URL contains "simple-form-demo"
  const currentUrl = await page.url();
  expect(currentUrl).toContain("simple-form-demo");

  // Create a string variable with message
  const message = "Welcome to LambdaTest";

  // Enter message in text box
  await page.fill(enterMessageInputSelector, message);

  // Click "Get Checked Value" button
  await page.click(buttonGetCheckedValue);

  // Get displayed message in right-hand panel
  const displayedMessage = await page.locator(getMessageTextSelector).textContent()

  // Validate messages are equal
  expect.soft(displayedMessage).toEqual(message);
});

test("Drag & Drop Slider to 95", async ({ page }) => {
  await page.goto(lambdaTestSeleniumPlaygroundUrl);

  // Scroll down to Progress Bars & Sliders section
  await page.locator("text=Progress Bars & Sliders").waitFor();

  // Click on Drag & Drop Sliders
  await page.locator("text=Drag & Drop Sliders").click();

  // Locate Default value 15 slider
  const slider = await page.locator(".range-slider").nth(0);

  // Check if it has the text "Default value 15"
  const sliderText = await slider.locator(".range-slider__label").innerText();
  expect(sliderText).toBe("Default value 15");

  // Click and hold slider handle
  const handle = await slider.locator(".range-slider__thumb");
  await handle.press down();

  // Drag handle relative to current position based on desired range value
  const trackWidth = await slider.locator(".range-slider__track").boundingBox().width;
  const desiredXOffset = trackWidth * (95 / 100);
  await slider.dragTo(handle, { sourcePosition: true, {x: desiredXOffset, y: 0 }});

  // Validate range value shows 95
  const rangeValue = await slider.locator(".range-slider__value").innerText();
  expect(rangeValue).toBe("95");

  // Release slider handle
  await handle.release();
});